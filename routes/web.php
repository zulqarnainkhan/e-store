<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('admin-layouts.master');
});

Route::get('/login', function () {
    return view('admin.login');
});

Route::prefix('user')->group(function () {
    //Sign Up
    Route::get('signup','UserController@signup');
    Route::post('register','UserController@register');

    // Sign In
    Route::get('signin','UserController@signin');
    Route::post('authentication','admin\loginController@authentication');

    //logout
    Route::get('logout','UserController@logout');

});
